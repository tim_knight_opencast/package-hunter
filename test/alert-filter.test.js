/* eslint-env mocha */

const AlertFilter = require('../src/alert-filter')
const assert = require('@sinonjs/referee').assert

describe('AlertFilter Test', function () {
  describe('isDiscardable', function () {
    describe('Bundler IPv6 connections', function () {
      describe('when alert is an IPv6 connection from Bundler', function () {
        it('returns true', function () {
          const alert = {
            output_fields: {
              'fd.name': '::ffff:172.17.0.2:56502->::e83c:ec1:f5ab:ffff:443',
              'proc.cmdline': 'bundle /usr/local/bin/bundle install --no-binstubs --path /tmp'
            }
          }
          const filter = new AlertFilter(alert)
          assert(filter.isDiscardable())
        })
      })

      describe('when connection is not initiated by Bundler', function () {
        it('returns false', function () {
          const alert = {
            output_fields: {
              'fd.name': '::ffff:172.17.0.2:56502->::e83c:ec1:f5ab:ffff:443',
              'proc.cmdline': 'curl https://::e83c:ec1:f5ab:ffff/'
            }
          }
          const filter = new AlertFilter(alert)
          assert(!filter.isDiscardable())
        })
      })

      describe('when alert is not an outgoing IPv6 connection', function () {
        it('returns false', function () {
          const alert = {
            output_fields: {
              'fd.name': 'Warp Core Breach',
              'proc.cmdline': 'bundle /usr/local/bin/bundle install --no-binstubs --path /tmp'
            }
          }
          const filter = new AlertFilter(alert)
          assert(!filter.isDiscardable())
        })
      })

      describe('when destination IPv6 address does not end with :ffff', function () {
        it('returns false', function () {
          const alert = {
            output_fields: {
              'fd.name': '::ffff:172.17.0.2:56502->::e83c:ec1:dead:beef:443',
              'proc.cmdline': 'bundle /usr/local/bin/bundle install --no-binstubs --path /tmp'
            }
          }
          const filter = new AlertFilter(alert)
          assert(!filter.isDiscardable())
        })
      })
    })
  })
})
