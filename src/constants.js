'use strict'

const HUNTER_MANIFESTS = {
  dependency: {
    yarn: {
      manifestFile: 'package.json',
      runnerOpts: {
        cmd: ['yarn', 'add', '/dependency'],
        cpDest: '/dependency', // For docker
        workingDir: '/my-package' // For docker
      },
      dockerOpts: {
        dockerCpDestinationPath: '/dependency',
        dockerCreateOpts: {
          Cmd: ['yarn', 'add', '/dependency'],
          WorkingDir: '/my-package',
          HostConfig: {}
        }
      }
    }
  },
  project: {
    yarn: {
      manifestFile: 'package.json',
      runnerOpts: {
        cmd: ['yarn', 'install'],
        cpDest: '/my-app', // For docker
        workingDir: '/my-app' // For docker
      }
    },
    npm: {
      manifestFile: 'package.json',
      runnerOpts: {
        cmd: ['npm', 'install', '--unsafe-perm'],
        cpDest: '/my-app', // For docker
        workingDir: '/my-app' // For docker
      }
    },
    bundler: {
      manifestFile: 'Gemfile',
      runnerOpts: {
        cmd: ['bundle', 'install', '--no-binstubs', '--path', '/tmp'],
        cpDest: '/my-app', // For docker
        workingDir: '/my-app' // For docker
      }
    },
    maven: {
      manifestFile: 'pom.xml',
      runnerOpts: {
        cmd: ['mvn', 'install'],
        cpDest: '/my-app', // For docker
        workingDir: '/my-app' // For docker
      }
    }
  }
}

module.exports = {
  HUNTER_MANIFESTS
}
