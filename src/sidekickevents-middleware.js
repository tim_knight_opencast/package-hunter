'use strict'

const debug = require('debug')('pkgs:sidekickevents-middleware')
const express = require('express')

const AlertFilter = require('./alert-filter.js')
const opts = require('./opts')

function handleIncomingEvent (alert) {
  const conId = alert.output_fields && alert.output_fields['container.id']
  debug(`received an alert for container ${conId}`)
  if (!conId) {
    debug(`Received alert without container id: ${alert}`)
    return
  }

  const reqId = opts.pendingContainer[conId]
  if (!reqId) {
    debug(`Could not find request id for container with id ${conId}`)
    return
  }

  const filter = new AlertFilter(alert)
  if (filter.isDiscardable()) {
    debug(`Discarding alert: ${alert.output}`)
    return
  }

  opts.jobStore[reqId].result.push(alert)
}

function sidekickeventsMiddleware (req, res) {
  handleIncomingEvent(req.body)

  res.status(200).send('ok')
}

function setupSidekickEventListener (app) {
  app.post('/internal/sidekick', express.json(), express.urlencoded({ extended: true }), sidekickeventsMiddleware)
}

module.exports = setupSidekickEventListener
