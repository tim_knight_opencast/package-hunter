'use strict'

const { spawn } = require('child_process')
const debug = require('debug')('pkgs:util')

const CMD_TIMEOUT = 60000

async function spawnHelper (cmd, args, opts) {
  debug(`executing ${cmd} with ${args}`)
  const proc = spawn(cmd, args, opts)

  let out = ''
  let err = ''
  let code

  proc.stdout.setEncoding('utf8')
  proc.stdout.on('data', function (data) {
    data = data.toString()
    out += data
  })

  proc.stderr.setEncoding('utf8')
  proc.stderr.on('data', function (data) {
    err += data
  })

  await new Promise(function (resolve, reject) {
    proc.on('close', x => {
      code = x
      resolve()
    })

    setTimeout(() => {
      reject(new Error(`Timed out waiting for ${cmd}.\n\tstdout=${out}\n\tstderr=${err}`))
    }, CMD_TIMEOUT)
  })

  return { out: out.trim(), err: err.trim(), code }
}

function createDockerContainerOpts (runnerOpts) {
  return {
    dockerCpDestinationPath: runnerOpts.cpDest,
    dockerCreateOpts: {
      Cmd: runnerOpts.cmd,
      WorkingDir: runnerOpts.workingDir,
      HostConfig: {
        AutoRemove: true
      }
    }
  }
}

module.exports = {
  spawnHelper,
  createDockerContainerOpts
}
