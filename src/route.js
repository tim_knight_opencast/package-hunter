'use strict'

const express = require('express')

const conf = require('./config')
const createAuthMiddleware = require('./auth-middleware')

module.exports = (middlewareFactory) => {
  const router = express.Router()

  router.post('*', createAuthMiddleware(conf.users))
  router.post('/dependency/yarn', middlewareFactory('dependency', 'yarn'))
  router.post('/project/yarn', middlewareFactory('project', 'yarn'))
  router.post('/project/npm', middlewareFactory('project', 'npm'))
  router.post('/project/bundler', middlewareFactory('project', 'bundler'))
  router.post('/project/maven', middlewareFactory('project', 'maven'))

  return router
}
