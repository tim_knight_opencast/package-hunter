const debug = require('debug')('pkgs:metrics')

const { collectDefaultMetrics, register } = require('prom-client')

module.exports = function () {
  collectDefaultMetrics({})

  return async function (req, res) {
    debug('GET /metrics')

    res.set('Content-Type', register.contentType)
    res.end(await register.metrics())
  }
}
